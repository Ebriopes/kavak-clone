import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CavakCoverComponent } from './cavak-cover.component';

describe('CavakCoverComponent', () => {
  let component: CavakCoverComponent;
  let fixture: ComponentFixture<CavakCoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CavakCoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CavakCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
