import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-logo-svg',
  templateUrl: './logo-svg.component.svg',
  styleUrls: ['./logo-svg.component.css']
})
export class LogoSvgComponent {

  @Input() fillColor: string;
  
}
